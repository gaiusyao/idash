# iDash

Gaius's Dash App Gallery

- [dash-xiaomi-2018](https://gitlab.com/gaiusyao/idash/tree/master/dash-xiaomi-2018): 小米集团 2018 年财报
- [getting-started](https://gitlab.com/gaiusyao/idash/tree/master/getting-started): 官方教程

可通过以下步骤使用本项目，首先克隆本项目：

``` 
git clone https://gitlab.com/gaiusyao/idash.git
```

接着构建虚拟环境（非必需），并安装依赖：

```
# 使用 conda 构建虚拟环境
conda create -n idash python=3.6 

# 启动虚拟环境
activate idash

# 安装依赖
pip install -r requirements.txt
```

然后启动想试用的 dash，这里以 `dash-xiaomi-2018` 为例：

```
# 进入同名文件夹
cd dash-xiaomi-2018

# 启动应用
python app.py
```

最后在浏览器输入 `http://127.0.0.1:8050/dash-xiaomi-2018` 以访问。

> Developed by Dash & Plotly