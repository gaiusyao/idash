# coding: utf-8

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go

from components import Header, make_dash_table, print_button

import pandas as pd


app = dash.Dash(__name__)
server = app.server

# Read data
df_core_index = pd.read_csv('data/df_core_index.csv')
df_gross_profit = pd.read_csv('data/df_gross_profit.csv')
df_stock = pd.read_csv('data/df_stock_1810.csv')
df_idc_2018q4 = pd.read_csv('data/df_idc_2018q4.csv')
df_iot_trend = pd.read_csv('data/df_iot_trend.csv')
df_internet_service_other = pd.read_csv('data/df_internet_service_other.csv')


# Page Overview
overview = html.Div([  

    print_button(),

    html.Div([

        Header(),

        # Row 1
        html.Div([

            html.Div([

                html.H6("全年营收、利润增长",
                        className="gs-header gs-text-header padded"),

                html.Div([

                    html.Div([
                        html.Br([]),

                        html.P("全年收入 1749 亿元"),
                        html.P("同比增长",
                               className="theme-text no-margin"),
                        html.P("52.6%",
                               className="theme-text no-margin big-number")
                    ], className="six columns"),

                    html.Div([
                        html.Br([]),

                        html.P("整后净利润 86 亿元"),
                        html.P("同比增长",
                               className="theme-text no-margin"),
                        html.P("59.5%",
                               className="theme-text no-margin big-number")
                    ], className="six columns"),

                ], className="row "),

            ], className="six columns"),

            html.Div([
                html.H6(["核心指标（单位：百万元）"],
                        className="gs-header gs-table-header padded"),
                html.Table(make_dash_table(df_core_index))
            ], className="six columns"),

        ], className="row"),

        # Row 2
        html.Div([

            html.Div([

                html.H6('智能手机仍是营收的主要来源（单位：千元）',
                        className="gs-header gs-text-header padded"),

                dcc.Graph(
                    id="graph-pie-revenue",
                    figure={
                        'data': [
                            go.Pie(
                                labels=['智能手机', 'IoT与生活消费品',
                                        '互联网服务', '其它'],
                                values=[113800386, 43816885, 15955558, 1342596],
                                marker=dict(
                                    colors=['rgba(255, 127, 0, 0.7)', 'rgba(255, 127, 0, 0.5)',
                                            'rgba(255, 127, 0, 0.3)', 'rgba(204, 204, 204, 0.7)']
                                ),
                                hoverinfo='label+value+percent',
                            )
                        ],
                        'layout': go.Layout(
                            height=240,
                            width=340,
                            hovermode="closest",
                            legend={
                                "orientation": "h",
                                "xanchor": "right",
                                "yanchor": "top",
                            },
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 8,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),
                            
            html.Div([

                html.H6("近半的毛利润由互联网服务贡献（单位：千元）",
                        className="gs-header gs-table-header padded"),

                dcc.Graph(
                    id="graph-pie-gross-profit",
                    figure={
                        'data': [
                            go.Pie(
                                labels=['智能手机', 'IoT与生活消费品', '互联网服务', '其它'],
                                values=[7043259, 4510751, 10271702, 366227],
                                marker=dict(
                                    colors=['rgba(255, 127, 0, 0.7)', 'rgba(255, 127, 0, 0.5)',
                                            'rgba(255, 127, 0, 0.3)', 'rgba(204, 204, 204, 0.7)']
                                ),
                            )
                        ],
                        'layout': go.Layout(
                            height=240,
                            width=340,
                            hovermode="closest",
                            legend={
                                "orientation": "h",
                                "xanchor": "right",
                                "yanchor": "top",
                            },
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 8,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),

        ], className="row"),

        # Row 3
        html.Div([

            html.Div([

                html.H6('海外市场收入占比大幅提升（2017 VS 2018）',
                        className="gs-header gs-table-header padded"),

                dcc.Graph(
                    id="grpah-donut-oversea",
                    figure={
                        'data': [
                            go.Pie(
                                values=[28, 72],
                                labels=['2017海外市场收入', '2017国内市场收入'],
                                marker=dict(
                                    colors=[
                                        'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                ),
                                hoverinfo='label+percent',
                                hole=.8,
                                domain={"x": [0, .48]},
                                name='2017年海外市场收入',
                                textinfo="none",
                            ),
                            go.Pie(
                                values=[40, 60],
                                labels=['2018海外市场收入', '2018国内市场收入'],
                                marker=dict(
                                    colors=[
                                        'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                ),
                                hoverinfo='label+percent',
                                hole=.8,
                                domain={"x": [.52, 1]},
                                name='2018年海外市场收入',
                                textinfo="none",
                            )
                        ],
                        'layout': go.Layout(
                            height=180,
                            width=340,
                            hovermode="closest",
                            annotations=[
                                {
                                    'font': {
                                        'size': 48
                                    },
                                    'showarrow': False,
                                    'text': '28%',
                                    'x': 0.1,
                                    'y': 0.5,
                                },
                                {
                                    'font': {
                                        'size': 48
                                    },
                                    'showarrow': False,
                                    'text': '40%',
                                    'x': 0.9,
                                    'y': 0.5,
                                },
                            ],
                            showlegend=False,
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 8,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                ),

                html.P("2018年海外市场收入700亿元，同比增长118.1%，占总收入40%，对总收入增长的贡献大。",
                        className="nomargin"),

            ], className="six columns"),

            html.Div([

                html.H6("股价走势持续走低（截至2019-03-22）",
                        className="gs-header gs-table-header padded"),

                dcc.Graph(
                    id='graph-line-stock',
                    figure={
                        'data': [
                            go.Scatter(
                                x=df_stock['Time'],
                                y=df_stock['Closed Price'],
                                fill='tozeroy',
                                line=dict(
                                    color='rgba(255, 127, 0, 0.7)'
                                ),
                            )
                        ],
                        'layout': go.Layout(
                            xaxis={
                                'visible': False,
                            },
                            height=240,
                            width=340,
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 32,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),

        ], className="row"),

        # Row 4
        html.Div([

            html.H6("小结",
                    className="gs-header gs-text-header padded"),

            html.Br([]),

            html.P("财报透露的信息整体偏乐观，收入和利润均有较高增长，但也存在不少隐忧，具体分析详见各业务页面。"),

        ], className="row"),

    ], className="subpage")

], className="page")


# Page Smart-Phone
smartPhone = html.Div([

    print_button(),

    html.Div([

        Header(),

        # Row 1
        html.Div([

            html.H6("智能手机业务逆势增长，毛利率不升反降",
                    className="gs-header gs-text-header padded"),

            html.Div([
                html.Div([
                    html.Br([]),

                    html.P("全球智能手机出货量减少"),
                    html.P("同比下滑",
                           className="theme-text no-margin"),
                    html.P("-4.1%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("小米手机出货 1.187 亿台"),
                    html.P("同比增长",
                           className="theme-text no-margin"),
                    html.P("30%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("小米手机收入 1138 亿元"),
                    html.P("同比增长",
                           className="theme-text no-margin"),
                    html.P("41.3%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("毛利率较去年下降"),
                    html.P("2017年8.8%",
                           className="theme-text no-margin"),
                    html.P("6.2%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),
            ], className="row "),

        ], className="row"),

        # Row 2
        html.Div([

            html.Div([

                html.H6("国内智能手机市场份额跌至10%",
                        className="gs-header gs-text-header padded"),

                dcc.Graph(
                    id="graph-line-market-share",
                    figure={
                        'data': [
                            go.Scatter(
                                name='华为',
                                x=['Q1', 'Q2', 'Q3', 'Q4'],
                                y=[24.2, 27.2, 24.6, 29.0],
                                marker=dict(
                                    color='rgba(207, 17, 43, 0.7)',
                                ),
                            ),
                            go.Scatter(
                                name='OPPO',
                                x=['Q1', 'Q2', 'Q3', 'Q4'],
                                y=[18.9, 20.2, 20.4, 19.6],
                                marker=dict(
                                    color='rgba(2, 103, 46, 0.7)',
                                ),
                            ),
                            go.Scatter(
                                name='vivio',
                                x=['Q1', 'Q2', 'Q3', 'Q4'],
                                y=[16.3, 19.0, 21.7, 18.8],
                                marker=dict(
                                    color='rgba(67, 88, 235, 0.7)',
                                ),
                            ),
                            go.Scatter(
                                name='小米',
                                x=['Q1', 'Q2', 'Q3', 'Q4'],
                                y=[15.1, 13.8, 13.6, 10.0],
                                marker=dict(
                                    color='rgba(255, 127, 0, 0.7)',
                                ),
                            ),
                            go.Scatter(
                                name='苹果',
                                x=['Q1', 'Q2', 'Q3', 'Q4'],
                                y=[11.3, 6.7, 7.4, 11.5],
                                marker=dict(
                                    color='rgba(119, 119, 119, 0.7)',
                                ),
                            ),
                        ],
                        'layout': go.Layout(
                            height=240,
                            width=340,
                            hovermode="closest",
                            legend={
                                "orientation": "h",
                                "xanchor": "right",
                                "yanchor": "top",
                            },
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 8,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),


            html.Div([
                html.H6(["2018Q4同比大幅下滑"],
                    className="gs-header gs-table-header padded"),
                html.Table(make_dash_table(df_idc_2018q4)),
                html.P("2018年第四季度，国内智能手机市场同比呈下滑趋势，除华为仍有较大幅度增长，蓝、绿厂仅有小幅增长，苹果、小米以及其它厂商同比均大幅下滑。",
                        className="no-margin"),
            ], className="six columns"),

        ], className="row"),

        # Row 3
        html.Div([

            html.Div([

                html.H6("2000元以上手机收入占比超三成",
                        className="gs-header gs-text-header padded"),

                dcc.Graph(
                    id="graph-pie-2000-phone",
                    figure={
                        'data': [
                            go.Pie(
                                values=[31.8, 68.2],
                                labels=['2000元及以上手机收入', '2000元以下手机收入'],
                                marker=dict(
                                    colors=['rgba(255, 127, 0, 0.7)',
                                            'rgba(204, 204, 204, 1)']
                                ),
                                hoverinfo='label+percent',
                                hole=.8,
                                name='2000元及以上手机收入占比',
                                textinfo="none",
                            )
                        ],
                        'layout': go.Layout(
                            height=240,
                            width=340,
                            hovermode="closest",
                            annotations=[
                                {
                                    'font': {
                                        'size': 48
                                    },
                                    'showarrow': False,
                                    'text': '31.8%',
                                    'x': 0.5,
                                    'y': 0.5,
                                },
                            ],
                            showlegend=False,
                            margin={
                                "r": 8,
                                "t": 16,
                                "b": 24,
                                "l": 8,
                            },
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),


            html.Div([
                html.H6(["小结"],
                    className="gs-header gs-table-header padded"),
                html.Br([]),
                html.P("智能手机业务无疑是小米集团的重中之重，从2018年整体趋势来看，小米手机仍能逆势增长。"),
                html.P("但从国内市场看，小米手机下滑趋势明显，其整体增长主要得益于海外市场发力。"),
                html.P("值得注意的是，ASP（单台均价）增长17%，2000元以上手机收入占比超过三成，但由于基数太低，小米想站稳中高端市场仍需时日。"),
            ], className="six columns"),

        ], className="row"),

    ], className="subpage"),

], className= "page")


# Page Internet-of-Things
internetOfThings = html.Div([

    print_button(),

    html.Div([

        Header(),

        # Row 1
        html.Div([

            html.H6("IoT与生活消费产品全面增长，广阔天地大有可为",
                    className="gs-header gs-text-header padded"),

            html.Div([
                html.Div([
                    html.Br([]),

                    html.P("2018年总收入438亿元"),
                    html.P("同比增长",
                           className="theme-text no-margin"),
                    html.P("87%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("连接设备数1.51亿台"),
                    html.P("同比增长",
                           className="theme-text no-margin"),
                    html.P("193.2%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("小爱同学激活设备过亿"),
                    html.P("活跃用户数",
                           className="theme-text no-margin"),
                    html.P("3880万",
                           className="theme-text no-margin big-number")
                ], className="three columns"),

                html.Div([
                    html.Br([]),

                    html.P("小米电视出货840万台"),
                    html.P("同比增长",
                           className="theme-text no-margin"),
                    html.P("225%",
                           className="theme-text no-margin big-number")
                ], className="three columns"),
            ], className="row "),

        ], className="row"),

        # Row 2
        html.Div([
            html.Div([
                html.H6(["IoT及生活消费产品增长趋势（2017Q1~2018Q4）"],
                        className="gs-header gs-table-header padded"),
                html.Table(make_dash_table(df_iot_trend))
            ], className="twelve columns"),
        ], className="row"),

        # Row 3
        html.Div([
            html.Div([
                html.H6(["IoT核心用户群初步形成"],
                        className="gs-header gs-table-header padded"),

                html.Br([]),

                html.P("约230万人，拥有超过5个小米物联网设备"),
                html.P("同比增长",
                        className="theme-text no-margin"),
                html.P("109.1%",
                        className="theme-text no-margin big-number")
            ], className="six columns"),

            html.Div([
                html.H6(["小结"],
                        className="gs-header gs-table-header padded"),

                html.Br([]),

                html.P("不同于智能手机市场，IoT仍是蓝海。"),
                html.P("虽然目前IoT与生活消费品的毛利不高，但其拥有较高的增速，生态链建设和核心用户群培育已初见成效。"),
                html.P("如在未来两三年内，继续保持这样的增速，小米将有望建立市场地位和护城河，甚至降低对智能手机和互联网服务的依赖。"),
            ], className="six columns"),

        ], className="row"),

    ], className="subpage")

], className="page")


# Page Internet-Services
internetServices = html.Div([  

    print_button(),

    html.Div([

        Header(),

        # Row 1
        html.Div([

            html.Div([

                html.H6("互联网业务增长迅速，对广告业务依赖大",
                        className="gs-header gs-text-header padded"),

                html.Div([

                    html.Div([
                        html.Br([]),

                        html.P("全年收入 160 亿元"),
                        html.P("同比增长",
                               className="theme-text no-margin"),
                        html.P("61.2%",
                               className="theme-text no-margin big-number")
                    ], className="six columns"),

                    html.Div([
                        html.Br([]),

                        html.P("广告业务营收101亿元"),
                        html.P("占互联网业务总收入",
                               className="theme-text no-margin"),
                        html.P("63%",
                               className="theme-text no-margin big-number")
                    ], className="six columns"),

                ], className="row "),

            ], className="six columns"),

            html.Div([
                html.H6(["2018Q4其它互联网业务增长情况"],
                        className="gs-header gs-table-header padded"),
                html.Table(make_dash_table(df_internet_service_other)),
            ], className="six columns"),

        ], className="row"),

        # Row 2
        html.Div([

            html.Div([

                html.H6("互联网收入受手机收入变化影响",
                        className="gs-header gs-text-header padded"),

                dcc.Graph(
                    id="graph-bar-line",
                    figure={
                        'data': [
                            go.Scatter(
                                x=['2018Q1', '2018Q2', '2018Q3', '2018Q4'],
                                y=[33.13, 39.58, 47.29, 40],
                                marker=dict(
                                    color='rgba(255, 127, 0, 0.7)',
                                ),
                                name='互联网收入（亿元）',
                            ),
                            go.Bar(
                                x=['2018Q1', '2018Q2', '2018Q3', '2018Q4'],
                                y=[232, 305, 349.8, 251],
                                marker=dict(
                                    color='rgba(204, 204, 204, 0.7)',
                                ),
                                yaxis='y2',
                                name='手机收入（亿元）',
                            )
                        ],
                        'layout': go.Layout(
                            height=240,
                            width=340,
                            hovermode="closest",
                            legend={
                                "orientation": "h",
                                "xanchor": "right",
                                "yanchor": "top",
                            },
                            margin={
                                "r": 48,
                                "t": 16,
                                "b": 24,
                                "l": 32,
                            },
                            yaxis=dict(
                                title='互联网收入'
                            ),
                            yaxis2=dict(
                                title='手机收入',
                                titlefont=dict(
                                    color='rgb(148, 103, 189)'
                                ),
                                tickfont=dict(
                                    color='rgb(148, 103, 189)'
                                ),
                                overlaying='y',
                                side='right'
                            ),
                        )
                    },
                    config={
                        'displayModeBar': False
                    }
                )

            ], className="six columns"),

            html.Div([

                html.H6(["MIUI活跃用户和每用户平均收入增长"],
                    className="gs-header gs-table-header padded"),

                html.Br([]),

                html.Div([
                    dcc.Graph(
                        id="graph-hbar-miui-user",
                        figure={
                            'data': [
                                go.Bar(
                                    x=[171, 242],
                                    y=['2018年', '2017年'],
                                    orientation='h',
                                    marker=dict(
                                        color=['rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                    ),
                                )
                            ],
                            'layout': go.Layout(
                                title='MIUI活跃用户（百万人）增长41.7%',
                                height=96,
                                width=340,
                                hovermode="closest",
                                showlegend=False,
                                margin={
                                    "r": 8,
                                    "t": 32,
                                    "b": 24,
                                    "l": 48,
                                },
                            )
                        },
                        config={
                            'displayModeBar': False
                        }
                    )
                ], className="row"),

                html.Div([
                    dcc.Graph(
                        id="graph-hbar-per-user",
                        figure={
                            'data': [
                                go.Bar(
                                    x=[57.9, 65.9],
                                    y=['2018年', '2017年'],
                                    orientation='h',
                                    marker=dict(
                                        color=[
                                                'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                    ),
                                )
                            ],
                            'layout': go.Layout(
                                title='每互联网用户所产生平均收入（元）增加',
                                height=96,
                                width=340,
                                hovermode="closest",
                                showlegend=False,
                                margin={
                                    "r": 8,
                                    "t": 32,
                                    "b": 24,
                                    "l": 48,
                                },
                            )
                        },
                        config={
                            'displayModeBar': False
                        }
                    )
                ], className="row"),

            ], className="six columns"),

        ], className="row"),     

        # Row 3
        html.Div([

            html.H6("小结",
                    className="gs-header gs-text-header padded"),

            html.Br([]),

            html.P("互联网业务虽然只占总收入不到一成，但却贡献了近半的毛利，可以说是小米的主要利润来源。且从长期来看，互联网业务仍将是小米利润的主要来源。"),
            html.P("但互联网业务有两大隐忧，一是过于依赖广告业务，二是与手机业务关联大。这两大隐忧一个影响了用户体验，甚至可能会“劝退”部分用户，另一个则极易受到目前手机业务下滑的影响。"),

        ], className="row"),        

    ], className="subpage")

], className="page")


other = html.Div([  # page other

    print_button(),

    html.Div([

        Header(),

        # Row 1
        html.Div([

            html.H6("研发投入大幅增加，但仍需加大投入",
                    className="gs-header gs-text-header padded"),

            html.Div([

                html.Div([

                    dcc.Graph(
                        id="graph-hbar-dev-spending",
                        figure={
                            'data': [
                                go.Bar(
                                    x=[58, 32, 21],
                                    y=['2018年', '2017年', '2016年'],
                                    orientation='h',
                                    marker=dict(
                                        color=[
                                            'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)',  'rgba(204, 204, 204, 1)']
                                    ),
                                )
                            ],
                            'layout': go.Layout(
                                height=160,
                                width=340,
                                hovermode="closest",
                                annotations=[{
                                    'font': {
                                        'size': 24
                                    },
                                    'showarrow': False,
                                    'text': '',
                                }],
                                showlegend=False,
                                margin={
                                    "r": 8,
                                    "t": 16,
                                    "b": 24,
                                    "l": 48,
                                },
                            )
                        },
                        config={
                            'displayModeBar': False
                        }
                    )

                ], className="six columns"),


                html.Div([
                    html.Br([]),
                    html.P("小米的研发投入在2018年达到了58亿，过去三年年复合增长率66.2%"),
                    html.P("这样的研发投入，虽然高于年研发投入40亿的绿厂，但与华为明显不是一个量级。"),
                    html.P("目前，行业竞争日益激烈，如不继续加大对研发的投入，必将导致产品缺乏竞争力和口碑的下滑。"),
                ], className="six columns"),

            ], className="row")

        ], className="row"),

        # Row 2
        html.Div([

            html.H6("线下渠道快速拓展，实际效果成疑",
                    className="gs-header gs-text-header padded"),

            html.Div([

                html.Div([

                    html.Br([]),

                    html.Div([
                        dcc.Graph(
                            id="graph-hbar-offline-stores",
                            figure={
                                'data': [
                                    go.Bar(
                                        x=[586, 281],
                                        y=['2018年', '2017年'],
                                        orientation='h',
                                        marker=dict(
                                            color=[
                                                'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                        ),
                                    )
                                ],
                                'layout': go.Layout(
                                    title='小米之家数量增加2倍',
                                    height=96,
                                    width=340,
                                    hovermode="closest",
                                    showlegend=False,
                                    margin={
                                        "r": 8,
                                        "t": 32,
                                        "b": 24,
                                        "l": 48,
                                    },
                                )
                            },
                            config={
                                'displayModeBar': False
                            }
                        )
                    ], className="row"),

                    html.Div([
                        dcc.Graph(
                            id="graph-hbar-authorized-store",
                            figure={
                                'data': [
                                    go.Bar(
                                        x=[1378, 62],
                                        y=['2018年', '2017年'],
                                        orientation='h',
                                        marker=dict(
                                            color=[
                                                'rgba(255, 127, 0, 0.7)', 'rgba(204, 204, 204, 1)']
                                        ),
                                    )
                                ],
                                'layout': go.Layout(
                                    title='小米之家数量增加22倍',
                                    height=96,
                                    width=340,
                                    hovermode="closest",
                                    showlegend=False,
                                    margin={
                                        "r": 8,
                                        "t": 32,
                                        "b": 24,
                                        "l": 48,
                                    },
                                )
                            },
                            config={
                                'displayModeBar': False
                            }
                        )
                    ], className="row"),

                ], className="six columns"),


                html.Div([
                    html.Br([]),
                    html.P("小米的线下渠道高速扩展，小米之家已达586个，小米授权店达到1378个。"),
                    html.P("但从绝对值来看，数量仍较少，与华为和蓝、绿厂相比仍有较大差距。"),
                    html.P("由于小米没有公布坪效等指标，并不清楚线下门店的实际效果，恐怕并没有看上去那么理想。"),
                ], className="six columns"),

            ], className="row")

        ], className="row")

    ], className="subpage")

], className="page")


noPage = html.Div([  # 404

    html.P(["404 Page not found"])

], className="no-page")


# Describe the layout, or the UI, of the app
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])

# Update page
# # # # # # # # #
# detail in depth what the callback below is doing
# # # # # # # # #


@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/dash-xiaomi-2018' or pathname == '/dash-xiaomi-2018/overview':
        return overview
    elif pathname == '/dash-xiaomi-2018/smart-phone':
        return smartPhone
    elif pathname == '/dash-xiaomi-2018/internet-of-things':
        return internetOfThings
    elif pathname == '/dash-xiaomi-2018/internet-services':
        return internetServices
    elif pathname == '/dash-xiaomi-2018/other':
        return other
    else:
        return noPage


# # # # # # # # #
# detail the way that external_css and external_js work and link to alternative method locally hosted
# # # # # # # # #
external_css = ["https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css",
                "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
                "https://fonts.googleapis.com/css?family=Raleway:400,300,600",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]

for css in external_css:
    app.css.append_css({"external_url": css})


external_js = ["https://code.jquery.com/jquery-3.2.1.min.js"]

for js in external_js:
    app.scripts.append_script({"external_url": js})


if __name__ == '__main__':
    app.run_server(debug=True)
