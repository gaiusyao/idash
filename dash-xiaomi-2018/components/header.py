import dash_html_components as html
import dash_core_components as dcc


def Header():
    return html.Div([
        get_header(),
        html.Br([]),
        get_menu()
    ])


def get_header():
    header = html.Div([

        html.Div([
            html.Img(src='https://logonoid.com/images/xiaomi-logo.png', height='40', width='40')
        ], className="one columns padded"),

        html.Div([
            html.H5(
                '《小米集团 2018 年财报》解读')
        ], className="eleven columns padded")

    ], className="row gs-header gs-text-header")
    return header


def get_menu():
    menu = html.Div([

        dcc.Link('总览   ', href='/dash-xiaomi-2018/overview', className="tab first"),

        dcc.Link('智能手机   ', href='/dash-xiaomi-2018/smart-phone', className="tab"),

        dcc.Link('IoT与生活消费产品   ', href='/dash-xiaomi-2018/internet-of-things', className="tab"),

        dcc.Link('互联网服务   ', href='/dash-xiaomi-2018/internet-services', className="tab"),

        dcc.Link('其它   ', href='/dash-xiaomi-2018/other', className="tab"),

    ], className="row ")
    return menu
