import dash_html_components as html


def print_button():
    printButton = html.A(['打印PDF'],
        className="button no-print print",
        style={'position': "absolute", 'top': '-40', 'right': '0'})
    return printButton
