## dash-xiaomi-2018

基于 **小米集团 2018 年财报** 的数据报告，数据主要来源于：
- [香港交易所 HKEX 数据](https://www.hkex.com.hk/Market-Data/Securities-Prices/Equities/Equities-Quote?sym=1810&sc_lang=zh_HK)
- [小米集团 2018 年财报](http://www3.hkexnews.hk/listedco/listconews/SEHK/2019/0319/LTN20190319788.pdf)
- [IDC](https://www.idc.com/) 报告数据

> 本项目主要参考了 [dash-vanguard-report](https://github.com/plotly/dash-vanguard-report)